# Backupkonzept m143

## 1. Einführung

### 1.1. Aufgabenstellung
Ich habe im Modul m143 den Auftrag erhalten ein Backupkonzept für eine fiktive Firma zu erstellen. Das Ziel ist es alle Aspekte aus der Kompetenzmatrix abzudeken. Auch wenn diese Szenario Realistisch sein könnte nehme ich an vielen Orten fiktive Zahlen, dass diese Projekt den Rahmen nicht sprengt. In den meisten Fällen sind diese Zahlen stimmen die Verhältnisse dieser Zahlen zu einander, sie wären aber in Wirklichkeit um ein Vielfaches grösser. Ich gebe an allen Orten, wo ich Grössenangaben mache, auch Zahlen, die echt sein können an.
### 1.2 Ausgangslage
Wir haben von der Firma X den Auftrag erhalten ein Backupkonzept zu erstellen. Bei der Firma handelt es sich um ein kleines Architekturbüro. Es gibt einen Geschäftsführer, zwei Architekten und eine administrativ angestellte Person. Sie verwenden einen Linux Server auf welchem zwei Virtuelle Maschinen laufen. Eine wird als reine Dateiablage verwendet, die andere als LDAP. Die Firma will sich im Zuge des neuen Backupkonzepts eine neuen Server anschaffen, der nur als Backupserver genutzt wird.
#### 1.2.1 Organigramm Firma
![FirmaOrganigramm](img/FirmaOrganigramm.png)
#### 1.2.2 Logischer Netzwerkplan Firma
![Logischer Netzwerkplan](img/LogischerNetzplan.png)
##### Geräte im Netz
| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|Router | gate01.firma.ch |  IP |10.0.0.1| 
|Switch | sw01.firma.ch | IP | 10.0.0.50|

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|Server 1 | srv01 | IP | 192.168.1.31|
| | | Subnetzmaske | 255.255.255.0
| | | OS | Ubuntu Server|
| | | Funktionen | LDAP, FileShare 2TB redundant|
| | | lokaler Benutzer | root | 

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|Server 2 | srv02 | IP | 192.168.1.32|
| | | Subnetzmaske | 255.255.255.0
| | | OS | TrueNas Scale|
| | | Funktionen | Backupserver |
| | | lokaler Benutzer | root |

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|PC01 | pc01 | IP | 192.168.2.101| 
| | | Subnetzmaske | 255.255.255.0|
| | | lokaler Benutzer | Administrator|
| | | OS | Windows 10 Pro| 

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|PC02 | pc02 | IP | 192.168.2.102| 
| | | Subnetzmaske | 255.255.255.0|
| | | lokaler Benutzer | Administrator|
| | | OS | Windows 10 Pro| 

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|PC03 | pc03 | IP | 192.168.2.103| 
| | | Subnetzmaske | 255.255.255.0|
| | | lokaler Benutzer | Administrator|
| | | OS | Windows 10 Pro| 

| Gerät | Hostname | Eigenschaften | Attribute
|---------|----------|------------|-----------| 
|PC04 | pc04 | IP | 192.168.2.104| 
| | | Subnetzmaske | 255.255.255.0|
| | | lokaler Benutzer | Administrator|
| | | OS | Windows 10 Pro|

##### Geräte ausserhalb des Netzes
Cloud Server für Offsite Backup auf welches zeitnah zugegriffen werden kann.

## 2. Konzept

### 2.1 Anforderungen

#### 2.1.1 Ziel der Datensicherung
Die Ziele dieses Backupkozepts sind:
-  ##### Datenverlust: 
   -  Die Sicherstellung, dass wichtige Daten nicht unwiederbringlich verloren gehen können, sei dies durch technische Fehler, menschliche Fehler, Naturkatastrophen oder böswillige Aktivitäten.
- ##### RTO:
  - Server: 24 Stunden
  
- ##### RPO: 
  - Server: 12 Stunden

#### 2.1.2 Zu sichernde Daten
- Projektdaten: CAD-Dateien, Skizzen, Fotos, Berechnungen
- Verwaltungsdaten: Rechnungen, Verträge, Lohn- und Gehaltsabrechnungen
- Systemdaten: Konfigurationsdateien, Software und Lizenzen

#### 2.1.3 Strategie:
- Server Full Backup: Der Server wird alle 24 Stunden auf den Backupserver gespiegelt. Dies geschieht um 02:00 Uhr Nachts. Täglich wird ein Snapshot des Full-Backup erstellt.
- Client Backup: Die Clients werden an und für sich nicht gebackupt, da wir eine Active Directory verwenden, welches separat gebackupt wird. Das heisst, falls ein Client einen Hardware Fehler erleidet, kann das Gerät repariert oder ausgetauscht werden und nach erfolgtem Setup die ganzen Nutzerdaten vom Active Directory aufgespielt werden. 
- Backup-Server Inkrementelles Backup in die Cloud: Der Backup-Server wird Wöchentlich in eine Google Cloud Storage Bucket gesichert. Dies dient dazu, dass der Backupserver auch komplett ausfallen kann, und relativ wenig Daten verloren gehen.

#### 2.1.4 Versionskontrolle:
  - Server: Täglich 00:00 Uhr ein Snapshot, 2 Monate Lifetime.

## 3. Praktische Umsetzung

### 3.1 Aufsetzen von TrueNas Scale

TrueNas Scale wird auf einem Dedizierten Server aufgesetzt. Es wird ein Storage Pool aufgesetzt für die Full Backups wie auch für die Snapshots. Angenommen die Architekturfirma hat zur Zeit 1.2 TB zu sichernde Daten, benötigen wir einen Pool der aus zwei 2TB Hard Drives besteht. Die zwei Hard Drives laufen in Raid 1 somit sind sie komplett gespiegelt, und es kann eine ausfallen ohne dass Datenverlust entsteht. Bei einem Datenzuwachs von 10% - 15% pro Jahr kann dieser Backupserver ohne weitere Hardware kaufen zu müssen zwei Jahre laufen. 

Um das ganze zu demonstrieren, habe ich auf meinem Proxmox Server eine TrueNas Scale Instanz installiert. Der Server, welcher gebackupt werden muss wird durch eine auf meinem PC laufende Ubuntu Server VM nachgestellt. Die Speicherkapazitäten sind zu Demonstrationszwecken stark verringert. 

Storage ZFS Pool im Mirror:
![Pool](img/Pool.png)

### 3.5 Einrichten des Backup Services

#### Rsync

Als Backup-Service wird Rsync verwendet. Dieser Service läuft auf TrueNas und holt sich die Daten vom Server. Das ganze ist über SSH mit einem Private/Public Key verschlüsselt. Diese kann man in TrueNas direkt unter Credentials -> Backup Credentials direkt erstellen, wie auch die SSH Verbindung. Es werden auf dem Daten-Server nur Files gelöscht, wenn sie innerhalb der 24 Stunden vom letzten zum nächsten backup erstellt und gelöscht werden.

Konfiguration Rsync:
![Rsync](img/Rsync.png)

Hier wurde der Einfachheit halber nur ein Backup über 24 Stunden gewählt. Es ist simpel z.B. mehrere Rsync Tasks zu erstellen, welche die Ordner in kürzeren Zeitintervallen backupt, dass weniger Datenverlust entsteht wenn untertags der Daten-Server aussteigt. Über das Feld "Remote Path" kann der Ordner, welcher gespiegelt wird gewählt werden. Und unter Schedule kann der Task automatisiert werden. In diesem Beispiel wird Täglich um 02:00Uhr Nachts das Backup durchgeführt.

#### Snapshots

Es wird Täglich ein Snapshot erstellt. So können ältere Arbeitsstände von Dateien wiederhergestellt werden. Diese Snapshots werden über 2 Monate behalten und danach gelöscht. Dies wird ebenfalls unter Schedule automatisiert.

Konfiguration Snapshot:
![SnapshotKonfig](img/SnapshotEdit.png)

Snapshot:
![Snapshot](img/Snapshot.png)

Hier ist auch ersichtlich, dass man auf einen bestimmten Snapshot zurücksetzen kann.

### 3.6 Aufsetzen Cloud

Es muss bei Google Cloud Services einen Google Cloud Storage Bucket eingerichtet werden:
![CloudBucket](img/GoogleBucket.png)

Danach musste bei Google einen Service account angelegt werden um TrueNas eine Authentifizierung bei GCS zu geben:
![GoogleKey](img/GoogleKey.png)

Dabei wurde ein JSON file generiert, welches man bei TrueNas beim erstellen des Cloud Sync Services hochladen musste.

Konfiguration Cloud Sync Task:
![CloudSyncTask](img/CloudSyncTask.png)

### 3.7 Einrichten Spiegelung in die Cloud

Unter Cloud Sync Task wird ein Task folgendermassen erstellt:
![CloudSyncTask](img/CloudSyncTask.png)

## 4. Testing

### 4.1 Test der Backup Services von Server zu Backupserver und Backupserver zu Cloud

Einerseits meldet TrueNas, dass das Backup stattgefunden hat:
![RsyncSuccess](img/RsyncSuccess.png)

Und auch, dass der Sync in die Cloud funktioniert hat:
![CloudSuccess](img/CloudSucess.png)

Man kann aber auch im Filesystem von TrueNas wie auch in der Cloud die gesicherten Dateien sehen. Hier ein Beispiel von TrueNas:
![RsyncTestSuccess](img/RsyncTestSucess.png)

### 4.2 Test wiederherstellen des Serves vom Backupserver

Zum Testen der Wiederherstellung auf den Server habe ich einfach eine neue Ubuntu Server VM erstellt. Und dann einen Rsync Task erstellt, der anstatt die Daten holt, auf den Server "pusht":
![DataPush](img/DataPush.png)

Und hier das Resultat vom Server:
![DataPushSuccess](img/DataPushSucess.png)

### 4.3 Alarmierung

Auf TrueNas ist es möglich, eine E-Mail Alarmierung einzurichten, oder auch über SNMP einen Monitoring Server anzusprechen. Aus Zeitlichen Gründen musste ich dies aber weglassen.

## 4. Reflexion

Ich bin nicht ganz zufrieden mit meinem Backupkonzept. Leider habe ich die Zeit falsch eingeschätzt und auch eingesetzt und bin somit nicht ganz zu allem gekommen was ich ausprobieren und zeigen wollte.  

## 5. Quellen

[https://www.truenas.com/docs/scale/scaletutorials/](https://www.truenas.com/docs/scale/scaletutorials/)
